package com.endava;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AwesomeController {

    @RequestMapping("/hello")
    public String helloWorld() {
        return "Hello, world!";
    }
}
